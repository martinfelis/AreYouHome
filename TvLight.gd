extends SpotLight

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var current_energy
var light_smoothness = 0.1

# Called when the node enters the scene tree for the first time.
func _ready():
	current_energy = self.light_energy
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	current_energy = rand_range(0.2, 15.0) * light_smoothness + (1.0 - light_smoothness) * current_energy
	self.light_energy = current_energy
#	pass

