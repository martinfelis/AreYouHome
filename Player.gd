extends Spatial

const GRAVITY = -24.8
var vel = Vector3()
const MAX_SPEED = 5
const JUMP_SPEED = 18
const ACCEL = 4.5

var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

var kinematic_body

# Called when the node enters the scene tree for the first time.
func _ready():
	kinematic_body = get_node("Body")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _physics_process(delta):
	process_input()
	process_movement(delta)
	
func process_input():

	# ----------------------------------
	# Walking
	dir = Vector3()
	var cam_xform = Transform()

	var input_movement_vector = Vector2()

	if Input.is_action_pressed("movement_forward"):
		input_movement_vector.y += 1
	if Input.is_action_pressed("movement_backward"):
		input_movement_vector.y -= 1
	if Input.is_action_pressed("movement_left"):
		input_movement_vector.x -= 1
	if Input.is_action_pressed("movement_right"):
		input_movement_vector.x += 1

	input_movement_vector = input_movement_vector.normalized()

	dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	dir += cam_xform.basis.x.normalized() * input_movement_vector.x
	# ----------------------------------

	# ----------------------------------

	# Interact with Lights	
	var interactibles = get_tree().get_nodes_in_group("Interactibles")
	var label = get_node("/root/Spatial/UI/InteractLabel") as Label
	label.text = ""
	for node in interactibles:
		var area = node.area
		if area and area.overlaps_body(kinematic_body):
			label.text = "[SPACE] to interact with " + area.get_parent().get_name()
			if Input.is_action_just_pressed("action"):
				node.interact()

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()

	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	
	vel = kinematic_body.move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
