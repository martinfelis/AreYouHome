extends Object

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var events = []
var event_index = 0
var current_time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func add_event(event):
	events.push_back(event)

func process(delta):
	current_time = current_time + delta
	if event_index < len(events):
		var event = events[event_index]
		if event.time <= current_time:
			if not event.trigger():
				return
			else:
				event_index = event_index + 1

func reset():
	event_index = 0
	current_time = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
