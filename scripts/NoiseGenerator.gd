extends RigidBody

export var noise_amount_by_player = 1.0
export var noise_amount_by_other = 1.0
export(NodePath) var touched_by_player_sound = null
export(NodePath) var touched_by_other_sound = null
export var collision_limit = -1

var _collision_count = 0
var _timer = null

func _ready():
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_setup")
	_timer.set_wait_time(3)
	_timer.set_one_shot(true)
	_timer.start()

func _setup():
	var error = connect("body_shape_entered", self, "_on_body_shape_entered")
	if error != OK:
		printerr("Could not connect")
	remove_child(_timer)

func _on_body_shape_entered(body_id, body, body_shape, local_shape):
	if collision_limit > -1 and _collision_count >= collision_limit:
		return
	_collision_count+=1
	
	var game = get_node("/root/Spatial/Game")
	
	var soundPath = null
	if body.get_parent().get_name() == "Player":
		soundPath = touched_by_player_sound
		game.noise_level += noise_amount_by_player		
	else:
		soundPath = touched_by_other_sound
		game.noise_level += noise_amount_by_other
		
	if soundPath:
		var sound = get_node(soundPath)
		if sound is AudioStreamPlayer and !sound.is_playing():
			sound.play()