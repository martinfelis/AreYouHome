extends MeshInstance

export(NodePath) var trigger

func _ready():
	var node = get_node(trigger)
	if node is Area:
		node.connect("body_entered", self, "_entered")
		node.connect("body_exited", self, "_exited")

func _entered(node):
	var name = node.get_parent().get_name()
	print(name+" entered")
	if name == "Player":
		_set_alpha(0.5)
	
func _exited(node):
	if node.get_parent().get_name() == "Player":
		_set_alpha(1.0)

func _set_alpha(amount):
	var material = get_surface_material(0)
	var color = material.albedo_color
	color.a = amount
	material.albedo_color = color
	print(material.resource_name+": "+String(color))