extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const SequenceNode = preload("SequenceNode.gd")
var root
var active_nodes = []
var started = false
var completed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func process(delta):
	if completed:
		return

	if !started:
		active_nodes.push_back(root)
		started = true
	
	var new_active_nodes = []
	
	for node in active_nodes:
		var node_completed = node.process(delta)
		
		if !node_completed:
			new_active_nodes.push_back(node)
		else:
			print ("[Seq] Done: %s" % node.comment)
			var next_nodes = node.get_next_nodes()
			if next_nodes.size() > 0:
				for next_node in next_nodes:
					print ("[Seq] New : %s" % next_node.comment)
					new_active_nodes.push_back(next_node)

	active_nodes = new_active_nodes.duplicate()
	
	if active_nodes.size() == 0:
		completed = true
			
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
