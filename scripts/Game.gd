extends Node

export var noise_level = 0.0
const NOISE_MAX = 10.0

# Objects relevant for interaction
var player_node
var bedroom_light_node
var bathroom_light_node
var kitchen_light_node
var bottles
var area_hallway
var fade_rect
var audio_player_door
var audio_player_character

# Audio Sources
var audio_source_player
var audio_source_doorbell


# Sequence Processor and nodes
var SequenceProcessor = load("res://scripts/SequenceProcessor.gd")
var WaitNode = load("res://scripts/SequenceNodes/Wait.gd")
var WaitForSignal = load("res://scripts/SequenceNodes/WaitForSignal.gd")
var WaitForPlayerAreaEntered = load("res://scripts/SequenceNodes/WaitForPlayerAreaEntered.gd")
var WaitForVisibilityOff = load("res://scripts/SequenceNodes/WaitForVisibilityOff.gd")
var EmitSound = load("res://scripts/SequenceNodes/EmitSound.gd")
var TriggerFade = load("res://scripts/SequenceNodes/TriggerFade.gd")
var BranchSequence = load("res://scripts/SequenceNodes/BranchSequence.gd")
var WaitForPlayerMultipleAreaEntered = load("res://scripts/SequenceNodes/WaitForPlayerMultipleAreaEntered.gd")
var sequence_processor = null

# Called when the node enters the scene tree for the first time.
func _ready():
	player_node = get_node("/root/Spatial/Player")
	
	bedroom_light_node = get_node("/root/Spatial/Bedroom/Light/Light")
	bathroom_light_node = get_node("/root/Spatial/Bathroom/Light/Light")
	kitchen_light_node = get_node("/root/Spatial/Kitchen/Light/Light")
	
	area_hallway = get_node("/root/Spatial/Hallway/Area")
	fade_rect = get_node("/root/Spatial/FadeRect")
	bottles = get_tree().get_nodes_in_group("bottle")
	
	assert(bedroom_light_node != null)
	assert(bathroom_light_node != null)
	assert(kitchen_light_node != null)
	
	audio_source_player = player_node.get_node("Body/AudioPlayer")
	audio_source_doorbell = get_node("/root/Spatial/Hallway/AudioPlayer")
	assert (audio_source_player != null)
	assert (audio_source_doorbell != null)

	sequence_processor = SequenceProcessor.new()

	prepare_sequence_1()	

func prepare_sequence_1():
	var start_node = TriggerFade.new(
		"Fade from black to clear",
		fade_rect,
		Color(0, 0, 0, 0),
		2
		)
	sequence_processor.root = start_node

	var wait_node = start_node.chain(
		WaitNode.new(
			"Am Anfang 2 Sekunden warten",
			2
			)
		)
	
	var sound_eingeschlafen = wait_node.chain(
		EmitSound.new(
			"Bin wieder eingeschlafen",
			audio_source_player,
			preload("res://audio/player/figureingeschlafen1_e.ogg")
			)
		)
		
	var player_entered_hallway = sound_eingeschlafen.chain(
		WaitForPlayerAreaEntered.new(
			"Warten bis Spieler im Flur",
			area_hallway
		)
	)
	
	var wait = player_entered_hallway.chain(
		WaitNode.new(
			"Kurz warten nachdem Spieler im Flur.",
			0.1
			)
	)

	var sound_klingel = wait.chain(
		EmitSound.new(
			"Klingelt die Türe",
			audio_source_player,
			preload("res://audio/door/tuerklingel1.ogg")
		)
	)
	
	wait = sound_klingel.chain(
		WaitNode.new(
			"Warten nach dem Klingeln.",
			2
			)
	)
	
	var sound_the_light = wait.chain(
		EmitSound.new(
			"Oh, das Licht!",
			audio_source_player,
			preload("res://audio/player/figurlicht1_e.ogg")
		)
	)
	
	var wait_visibility = WaitForVisibilityOff.new(
			"Bedroom light off",
			[bedroom_light_node, kitchen_light_node, bathroom_light_node]
			)
	
	var collide_with_bottles = WaitForPlayerMultipleAreaEntered.new(
			"Player collides with a bottle",
			bottles
			)
	
	var branch_end_or_bottle_collide = sound_the_light.chain(
		BranchSequence.new(
			"Branch: switch of light or bottle collision",
			wait_visibility,
			collide_with_bottles
			)
		)
	
	wait = collide_with_bottles.chain(
		WaitNode.new(
			"Pause after bottle collision.",
			2
			)
	)
	
	var door_knocking = wait.chain(
		EmitSound.new(
			"Knocking on the door",
			audio_source_doorbell,
			preload("res://audio/door/tuernock3.ogg")
		)
	)
	
	var fade_to_black = wait_visibility.chain(
		TriggerFade.new(
			"Fade from black to clear",
			fade_rect,
			Color(0, 0, 0, 1),
			2
		)
	)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var noise_label = get_node("/root/Spatial/DebugUI/Noise")
	noise_label.text = "Noise: " + str(noise_level)
	
#	var noise_slider = get_node("/root/Spatial/DebugUI/NoiseSlider")
#	if (noise_slider):
#		noise_slider.value = 100 * noise_level / NOISE_MAX + rand_range(-1, 1)
	
	sequence_processor.process(delta)


func _on_Light_visibility_changed(extra_arg_0):
	pass # Replace with function body.

