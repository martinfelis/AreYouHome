extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

export var completed = false
var next = null
export var comment = ""

func _init(_comment=""):
	comment = _comment

# When it returns false the sequence is halted. Once it returns
# true the sequence continues.
func process(delta):
	print("Processing node %s" % name)
	completed = true
	return completed

func chain(other_node):
	next = other_node
	return other_node
	
func get_next_nodes():
	var result = []
	if next != null:
		result.push_back(next)

	return result