extends "../SequenceNode.gd"

export var target_color = Color(255, 255, 255, 0)
export var duration = 1

var started = false
var fade_state = 0
var fade_rect
var source_color

func _init(_comment="",_fade_rect=null,_target_color=null,_duration=1).(_comment):
	target_color = _target_color
	duration = _duration
	fade_rect = _fade_rect
	
	assert(fade_rect != null)

# When it returns false the sequence is halted. Once it returns
# true the sequence continues.
func process(delta):
	if completed:
		return true
	
	if fade_state == 0:
		source_color = fade_rect.color
		completed = false

	fade_state = fade_state + delta/duration	
	
	if fade_state >= 1:
		fade_state = 1
		completed = true
	
	fade_rect.color = source_color.linear_interpolate(target_color, fade_state)
	
	return completed
