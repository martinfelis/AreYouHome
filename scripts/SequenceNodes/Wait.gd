extends "../SequenceNode.gd"

export var duration = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _init(_comment="",_duration = 0).(_comment):
	duration = _duration

# When it returns false the sequence is halted. Once it returns
# true the sequence continues.
func process(delta):
	duration = duration - delta	

	completed = false
	if duration <= 0:
		completed = true
#	else:
#		print ("Still waiting for %f seconds" % duration)

	return completed
