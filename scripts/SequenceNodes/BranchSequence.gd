extends "../SequenceNode.gd"

var branch1
var branch2

func _init(_comment="",_branch1 = null, _branch2 = null).(_comment):
	branch1 = _branch1
	branch2 = _branch2
	
	assert(branch1 != null)
	assert(branch2 != null)

func process(delta):
	return true
	
func get_next_nodes():
	var result = []
	
	result.push_back(branch1)	
	result.push_back(branch2)	
	
	return result