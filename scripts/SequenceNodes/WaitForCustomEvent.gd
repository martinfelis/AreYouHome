extends Node

var emitted = false

var event_check_func

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _init(_comment="", _event_check_callback).(_comment):
	event_check_func = _event_check_callback
	
func process(delta):
	if !emitted:
		emitted = event_check_callback()

	return emitted