extends "../SequenceNode.gd"

var emitted = false
var areas = null

func _init(_comment="", _areas=null).(_comment):
	areas = _areas
	assert (areas != null)

	for area in areas:
		area.connect("body_entered", self, "_on_body_entered")

func _on_body_entered(node):
	if node.get_node("../").get_name() == "Player":
		emitted = true

func process(delta):
	return emitted