extends "../SequenceNode.gd"

var emitted = false
var area = null

func _init(_comment="", _area=null).(_comment):
	area = _area
	assert (area != null)

	area.connect("body_entered", self, "_on_body_entered")

func _on_body_entered(node):
	if node.get_node("../").get_name() == "Player":
		emitted = true

func process(delta):
	return emitted
	