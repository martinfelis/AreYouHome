extends "res://scripts/SequenceNode.gd"

var emitted = false

var object
var signal_name

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_signal():
	print ("received signal")
	emitted = true

func _init(_comment="", _object = null, _signal_name="").(_comment):
	object = _object
	signal_name = _signal_name
	
	object.connect(signal_name, self, "_on_signal")
	
func process(delta):
	return emitted

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
