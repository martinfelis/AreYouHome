extends "../SequenceNode.gd"

var emitted = false
var nodes = []

func _init(_comment="", _nodes=null).(_comment):
	nodes = _nodes
	assert (len(nodes) != 0)

func process(delta):
	var have_active_light = false
	for node in nodes:
		if node.is_visible() == true:
			have_active_light = true
		
	return !have_active_light
	