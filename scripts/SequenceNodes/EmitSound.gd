extends "../SequenceNode.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var audio_player
var sample

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _init(_comment="",_audio_player=null, _sample=null).(_comment):
	audio_player = _audio_player
	sample = _sample
	
	assert(audio_player != null)
	assert(sample != null)

# When it returns false the sequence is halted. Once it returns
# true the sequence continues.
func process(delta):
	audio_player.stop()
	audio_player.stream = sample
	audio_player.play()
	
	completed = true
	return completed
