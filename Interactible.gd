extends Spatial

export var noise = 1.0
var area = null
var light = null
var sound_container = null

func _ready():
	area = get_node("Area")
		
	if get_node("Light"):
		light = get_node("Light")
	if get_node("Sounds"):
		sound_container = get_node("Sounds")

func _physics_process(delta):
	var add_noise = false
	if sound_container:
		for sound in sound_container.get_children():
			if sound is AudioStreamPlayer:
				if sound.playing:
					add_noise = true
	if add_noise:
		var game = get_node("/root/Spatial/Game")
		game.noise_level += noise * delta

func interact():
	if light:
		light.set_visible(!light.is_visible())
	if sound_container:
		for sound in sound_container.get_children():
			if sound is AudioStreamPlayer:
				if sound.playing:
					sound.stop()
				else:
					sound.play()