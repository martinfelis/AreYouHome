extends "Event.gd"

var sample
var audio_player
	
func trigger():
	print ("Triggering SoundEvent sound %s" % sample.resource_path)
	
	audio_player.stop()
	audio_player.stream = sample
	audio_player.play()
	
	return true

