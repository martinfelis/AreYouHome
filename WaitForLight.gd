extends "Event.gd"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var light = null
var goal_visibility = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func trigger():
	if light.is_visible() == goal_visibility:
		print ("Light visibility reached %s" % String(goal_visibility))
		return true
	return false